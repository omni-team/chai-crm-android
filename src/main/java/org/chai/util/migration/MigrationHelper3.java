package org.chai.util.migration;

import android.database.sqlite.SQLiteDatabase;
import org.chai.model.SummaryReportDao;

/**
 * Created by victor on 22-Apr-15.
 */
public class MigrationHelper3 extends MigratorHelper {
    @Override
    public void onUpgrade(SQLiteDatabase db) {
        SummaryReportDao.createTable(db, true);
    }
}
