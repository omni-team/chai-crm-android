package org.chai.util.migration;

import android.database.sqlite.SQLiteDatabase;
import org.chai.model.CustomerDao;
import org.chai.model.SummaryReportDao;
import org.chai.model.TaskOrderDao;

/**
 * Created by victor on 21-Apr-15.
 */
public class MigrationHelper2 extends MigratorHelper {
    @Override
    public void onUpgrade(SQLiteDatabase db) {
        executeAlterChangeSets(db, TaskOrderDao.TABLENAME, CustomerDao.Properties.IsDirty.columnName, TYPE_INTEGER);
        executeAlterChangeSets(db, TaskOrderDao.TABLENAME, CustomerDao.Properties.SyncronisationStatus.columnName, TYPE_INTEGER);
        executeAlterChangeSets(db, TaskOrderDao.TABLENAME, CustomerDao.Properties.SyncronisationMessage.columnName, TYPE_TEXT);
        executeAlterChangeSets(db, TaskOrderDao.TABLENAME, CustomerDao.Properties.DateCreated.columnName, TYPE_INTEGER);
        executeAlterChangeSets(db, TaskOrderDao.TABLENAME, CustomerDao.Properties.LastUpdated.columnName, TYPE_INTEGER);

    }
}
